import React from 'react';
import ScratchCode from './components/scratchsand'
import Firework from './components/Fireworks'

const App = () => {
  return (
    < div className='app-container'>
     
     <ScratchCode />
     <Firework/>
    </div>
  );
};

export default App;
